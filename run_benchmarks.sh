#!/bin/sh
DIR="$(cd "$(dirname "$0")" && pwd)"

#  Run different iterations of the benchmark program

# Args:
# Function: (0 - basic_op, 1 - special op)
# Filter: (0 - filter1, 1 - filter1_gpu, 2 - filter2, 3 - filter2_gpu)
# Num elements: Memory size - size of the container to process
# Granularity: Computational weight - number of iterations to be executed by each GPU thread.
# Num runs: Number of times the benchmark is executed (optional, default is 1)

FUNCTIONS=(0 1)

FILTERS_CPU=(0 2)
FILTERS_GPU=(1 3 4)

NELEMS=(100000 1000000 10000000 100000000)
GRANULARITIES=(50 500 5000)

#NELEMS_GPU=(35000000 50000000 75000000 100000000)
#GRANULARITIES_GPU=(3000 15000 30000)

NRUNS_CPU=5
NRUNS_GPU=10

# 1 file for each granularity value
OUTFILES_CPU=("cpu_results_1.txt" "cpu_results_2.txt" "cpu_results_3.txt")
OUTFILES_GPU=("gpu_results_1.txt" "gpu_results_2.txt" "gpu_results_3.txt")

# CPU
for FILENR in {0..2}; do
  OUTFILE=${OUTFILES_CPU[$FILENR]}

  printf "Function,Filter,Num Elems,Granularity,Num Runs,Avg Time\n"
  for NELEM in "${NELEMS[@]}"; do
    for FILTER in "${FILTERS_CPU[@]}"; do
      for FUNC in "${FUNCTIONS[@]}"; do
        printf "$FUNC,$FILTER,$NELEM,${GRANULARITIES[$FILENR]},$NRUNS_CPU,"
        $DIR/cmake-build-debug/src/benchmark $FUNC $FILTER $NELEM $GRANULARITIES[$FILENR] $NRUNS_CPU
      done
    done
  done | tee $OUTFILE
done

# GPU
for FILENR in {0..2}; do
  OUTFILE=${OUTFILES_GPU[$FILENR]}

  printf "Function,Filter,Num Elems,Granularity,Num Runs,Avg Time\n"
  for NELEM in "${NELEMS[@]}"; do
    for FILTER in "${FILTERS_GPU[@]}"; do
      for FUNC in "${FUNCTIONS[@]}"; do
        printf "$FUNC,$FILTER,$NELEM,${GRANULARITIES[$FILENR]},$NRUNS_GPU,"
        $DIR/cmake-build-debug/src/benchmark $FUNC $FILTER $NELEM $GRANULARITIES[$FILENR] $NRUNS_GPU
      done
    done
  done | tee $OUTFILE
done
