/**
 * Tests for functions
 */


#include "../cad_test.h"

#include <array>
#include <functional>
#include <algorithm>
#include <type_traits>

#include <skeletons.hpp>
#include <marrow/timer.hpp>

using namespace cad;


//template<typename T>
//bool bigger_than(T x, T val) {
//    return x > val;
//}

template<typename T>
__device__ __host__ bool bigger_than(T x, T val) {
    return x > val;
}


TEST(Filter1, BT10_1000) {

    constexpr unsigned size = 1000;
    std::array<int, size> a;

    std::fill(a.begin(), a.end(), 1);
    a[100] = 20;

    auto result = filter1(999, bigger_than<int>,  a, 10);

    std::array<int, size> expected;
    std::fill(expected.begin(), expected.end(), 999);
    expected[100] = 20;
    expect_container_eq(result, expected);
}

TEST(Filter2, BT10_1000) {

    constexpr unsigned size = 1000;
    std::array<int, size> a;

    std::fill(a.begin(), a.end(), 0);
    a[100] = 20;
    a[101] = 11;
    a[102] = 12;

    auto result = filter2(bigger_than<int>,  a, 10);

    expect_container_eq(result,
           std::array<located_value<int>, 3> {{ { 100, 20 }, {101, 11}, {102, 12} }});
}



/////////////// GPU

//template <typename T>
//__global__ void gpu_bigger_than(const unsigned size, T defaultVal, T* container, T x) {
//    int index = blockIdx.x * blockDim.x + threadIdx.x;
//
//    if(index < size){
//        container[index] = (container[index] > x) ? container[index] : defaultVal;
//    }
//}

//// Instead of kernel, function to be applied element by element

template<typename... Ts>
using filter_func = bool (*) (Ts...);

template <typename T>
__device__ filter_func<T, T> p_bigger_than = bigger_than<T>;



TEST(Filter1_gpu, BT10_1000) {

    constexpr unsigned size = 1000;
    std::array<int, size> a;

    std::fill(a.begin(), a.end(), 1);
    a[100] = 20;

//    auto result = filter1_gpu(999, gpu_bigger_than<int>,  a, 10);
    auto result = filter1_gpu(999, p_bigger_than<int>,  a, 10);

    std::array<int, size> expected;
    std::fill(expected.begin(), expected.end(), 999);
    expected[100] = 20;

    expect_container_eq(result, expected);
}





TEST(Filter2_gpu, BT10_1000) {

    constexpr unsigned size = 12;
    std::array<int, size> a = {12, 1, 11, 1, 1, 11, 11, -1, 1, 1, 11, 11};

//    using LocatedValue = my_located_value<int>;
    auto result = filter2_gpu(p_bigger_than<int>,  a, 10);

//    std::cout << "size: " << result.size();

    auto vals = result.values;
    auto coords = result.coordinates;

    for(int i = 0; i < vals.size(); i++){
        char buffer[50];
        sprintf(buffer, "{%ld, %d}", coords[i], vals[i]);
        std::cout << buffer << ", ";
    }

//    for(auto e : result){
//        char buffer[50];
//        sprintf(buffer, "{%ld, %d}", e.coordinate, e.value);
//        std::cout << e << ", ";
//    }


//    expect_container_eq(result, expected);
}


TEST(Filter2_gpu, BT10_10000) {
    constexpr unsigned size = 100000;
    std::array<int, size> a;

    std::fill(a.begin(), a.end(), 0);
    a[100] = 20;
    a[101] = 11;
    a[102] = 12;
    a[1001] = 111;

    auto result = filter2_gpu(p_bigger_than<int>,  a, 10);

    auto vals = result.values;
    auto coords = result.coordinates;

    for(int i = 0; i < vals.size(); i++){
        char buffer[50];
        sprintf(buffer, "{%ld, %d}", coords[i], vals[i]);
        std::cout << buffer << ", ";
    }

//    expect_container_eq(result,
//                        std::array<my_located_value<int>, 4> {{ { 100, 20 }, {101, 11}, {102, 12}, {1001, 111} }});
}


TEST(Filter2_gpu_async, BT10_12) {

    constexpr unsigned size = 12;
    std::array<int, size> a = {12, 1, 11, 1, 1, 11, 11, -1, 1, 1, 11, 11};

//    using LocatedValue = my_located_value<int>;
    auto result = filter2_gpu_async(p_bigger_than<int>,  a, 10);


    auto vals = result.values;
    auto coords = result.coordinates;

    for(int i = 0; i < result.size; i++){
        char buffer[50];
        sprintf(buffer, "{%ld, %d}", coords[i], vals[i]);
        std::cout << buffer << ", ";
    }



//    expect_container_eq(result, expected);
}



template <typename Container>
void scan_seq(Container& out, Container in)
{
    out[0] = 0;
    for(int i = 1; i < in.size(); i++)
            out[i] = out[i-1] + in[i-1];
}

TEST(Scan, BT10_1000) {


    constexpr unsigned size = (1<<21) - 35;
    constexpr unsigned size_in_bytes = size * sizeof(int);
    std::vector<int> a(size);
    std::fill(a.begin(), a.end(), 1);

    std::vector<int> expected(size);
    scan_seq(expected, a);

    int * d_a;
    cudaMalloc((void**) &d_a, size_in_bytes);
    cudaMemcpy(d_a, a.data(), size_in_bytes, cudaMemcpyHostToDevice);


    marrow::timer<> t;
    t.start();
        int * d_scan_output = scan_gpu(d_a, size);
    double elapsed = t.stop();
//    t.output_stats(std::cout);
    std::cout << elapsed  << " milliseconds\n\n";

    std::vector<int> result(size);

    cudaMemcpy(result.data(),d_scan_output, size_in_bytes, cudaMemcpyDeviceToHost);

    std::cout << "expected last elem: " << expected[size-1] << "\n";
    std::cout << "result's last elem: " << result[size-1] << "\n";
    std::cout << "---------------------------------------------\n";

    expect_container_eq(result, expected);
}

