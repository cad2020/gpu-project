#include <cstdio>
#include <vector>
#include <string>
#include <iostream>
#include <array>

#include <skeletons.hpp>
#include <marrow/timer.hpp>

using namespace std;


/////////////////
// The filter functions
/////////////////

template<typename... Ts>
using filter_func = bool (*) (Ts...);

template<typename T>
bool basic_op(T x, unsigned granularity) {

    int val = 0;
    for (unsigned i = 0; i < granularity; i++)
        val += i;

    return x > val;
}

template<typename T>
__device__  bool basic_op_gpu(T x, unsigned granularity) {

    int val = 0;
    for (unsigned i = 0; i < granularity; i++)
        val += i;

    return x > val;
}

template <typename T>
__device__ filter_func<T, unsigned> p_basic_op = basic_op_gpu<T>;


template<typename T>
bool sfu_op(T x, unsigned granularity) {

    int val = 0;
    for (unsigned i = 0; i < granularity; i++)
        val += sqrtf(i)/i;

    return x > val;
}

template<typename T>
__device__  bool sfu_op_gpu(T x, unsigned granularity) {

    int val = 0;
    for (unsigned i = 0; i < granularity; i++)
        val += sqrtf(i)/i;

    return x > val;
}

template <typename T>
__device__ filter_func<T, unsigned> p_sfu_op = sfu_op_gpu<T>;

/////////////////
// The main function
//   the benchmark receives four arguments
//      function: (0 - basic_op, 1 - special op)
//      filter: (0 - filter1, 1 - filter1_gpu, 2 - filter2, 3 - filter2_gpu, 4 - filter2_gpu_async)
//      nelems: Memory size - size of the container to process
//      granularity: Computational weight - number of iterations to be executed by each GPU thread.
//      nruns -- Number of times the benchmark is executed (optional, default is 1)
/////////////////

int main(int argc, char* argv[]) {
    if (argc < 5 || argc > 6) {
        printf("usage: %s function (0 - basic_op, 1 - special op) filter (0 - filter1, 1 - filter1_gpu, 2 - filter2, 3 - filter2_gpu, 4 - filter2_gpu_async) nelems granularity [nruns]\n", argv[0]);
        return 1;
    }

    unsigned char function = static_cast< unsigned char>(stoul (argv[1], nullptr,0));
    unsigned char filter = static_cast< unsigned char>(stoul (argv[2], nullptr,0));
    size_t nelems = stoul (argv[3], nullptr,0);
    unsigned granularity = stoul (argv[4], nullptr,0);
    unsigned nruns = argc == 3 ? 1 : stoul (argv[5], nullptr,0);

    auto in = make_shared<vector<float>>(nelems);

    marrow::timer<std::chrono::microseconds> t;

    if (function == 0) {
        for (unsigned i = 0; i < nruns; i++) {
            //printf ("Run number %u\n", i);
            std::fill(in->begin(), in->end(), 1);

            t.start();

            switch (filter) {
                case 0:
                    cad::filter1(999.0f, basic_op<float>, *in, granularity);
                    break;

                case 1:
                    cad::filter1_gpu(999.0f, p_basic_op<float>, *in, granularity);
                    break;

                case 2:
                    cad::filter2(basic_op<float>, *in, granularity);
                    break;

                case 3:
                    cad::filter2_gpu(p_basic_op<float>, *in, granularity);
                    break;

                case 4:
                    cad::filter2_gpu_async(p_basic_op<float>, *in, granularity);
                    break;

                default:
                    //printf("Unknown filter function\n");
                    return -1;
            }

            //CUT_CHECK_ERROR("test");


            t.stop();
        }

    }
    else {
        for (unsigned i = 0; i < nruns; i++) {
            //printf ("Run number %u\n", i);
            std::fill(in->begin(), in->end(), 1);

            t.start();

            switch (filter) {
                case 0:
                    cad::filter1(999.0f, sfu_op<float>, *in, granularity);
                    break;

                case 1:
                    cad::filter1_gpu(999.0f, p_sfu_op<float>, *in, granularity);
                    break;

                case 2:
                    cad::filter2(sfu_op<float>, *in, granularity);
                    break;

                case 3:
                    cad::filter2_gpu(p_sfu_op<float>, *in, granularity);
                    break;

                case 4:
                    cad::filter2_gpu_async(p_sfu_op<float>, *in, granularity);
                    break;

                default:
                    //printf("Unknown filter function\n");
                    return -1;
            }

            //CUT_CHECK_ERROR("test");

            t.stop();
        }
    }

    //t.average();
    //t.std_deviation();
    t.print_stats(cout, marrow::main_stage);
    printf("\n");
    //printf("\n\n");
}