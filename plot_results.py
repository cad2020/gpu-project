import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv
import os

FUNCTIONS = ["basic_op", "special_op"]
FILTERS = ["filter1", "filter1_gpu", "filter2", "filter2_gpu", "filter2_gpu_async"]

NELEMS = [100000, 1000000, 10000000, 100000000]


def plot_benchmark_results(filename):
    file = open(filename)
    reader = csv.reader(file, delimiter=',')

    granularity = 0
    is_cpu = False

    # Index mappings of filter_func and func to avg_times row
    indices = np.array([[0, 1], [0, 1], [2, 3], [2, 3], [4, 5]], np.int32)

    # 6 rows - all combinations between filters and functions
    # 4 columns - all num elem ticks
    avg_times = np.zeros((6,4))
    line_count = 0
    row_num = 0
    col_num = 0
    max_rows = 0
    for row in reader:
        func = int(row[0])
        filter_func = int(row[1])
        avg_time = float(row[5]) / 1e6 # convert microsseconds to seconds

        if line_count == 0:
            granularity = int(row[3])

            if filter_func == 0 or filter_func == 2:
                # It's a CPU results file
                is_cpu = True
                max_rows = 4

            else:
                # It's a GPU results file
                max_rows = 6
        
        avg_times[indices[filter_func, func], col_num] = avg_time

        line_count += 1
        row_num += 1

        if row_num >= max_rows:
            row_num = 0
            col_num += 1

    title = 'CPU, ' if is_cpu else 'GPU, '
    title += 'Granularity = {}'.format(granularity)

    figname = os.path.splitext(filename)[0] + '.png'

    plt.plot(NELEMS, avg_times[0], 'r-', label='filter1 - basic_op')
    plt.plot(NELEMS, avg_times[1], 'r--', label='filter1 - special_op')
    plt.plot(NELEMS, avg_times[2], 'g-', label='filter2 - basic_op')
    plt.plot(NELEMS, avg_times[3], 'g--', label='filter2 - special_op')

    if not is_cpu:
        # GPU results have filter2_async as extra
        plt.plot(NELEMS, avg_times[4], 'b-', label='filter2_async - special_op')
        plt.plot(NELEMS, avg_times[5], 'b--', label='filter2_async - special_op')

    plt.legend(loc='upper left')
    #plt.xticks(NELEMS)
    plt.gcf().axes[0].xaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    plt.xlabel('Num Elements')
    plt.ylabel('Time (seconds)')
    plt.title(title)
    #plt.show()
    plt.savefig(figname, format='png')
    plt.clf()


plot_benchmark_results('cpu_results_1.txt')
plot_benchmark_results('cpu_results_2.txt')
plot_benchmark_results('cpu_results_3.txt')
plot_benchmark_results('gpu_results_1.txt')
plot_benchmark_results('gpu_results_2.txt')
plot_benchmark_results('gpu_results_3.txt')