//
// Created by Hervé Paulino on 18/03/2020.
//

#ifndef CADPROJECT_CUDA_SKELETONS_HPP
#define CADPROJECT_CUDA_SKELETONS_HPP

#include <type_traits>

#include "located_value.hpp"
#include "gpu_support.hpp"
#include "scan_aux.hpp"

namespace cad {

//    constexpr auto BLOCK_SIZE = 256; // TODO é este o valor?

    /**
     * Function that provides the means to obtain the element to applied in a elementary map application
     * Only supports fundamental types and containers
     * @tparam T The type of the value to process
     */
    template <typename T, std::enable_if_t<std::is_fundamental<T>::value>* = nullptr>
    constexpr T argument_get(T argument, std::size_t index) {
        return argument;
    }

    template <typename T, std::enable_if_t<not std::is_fundamental<T>::value>* = nullptr>
    constexpr typename std::remove_reference_t<T>::value_type argument_get(T&& argument, std::size_t index) {
       return argument[index];
    }


    /**
     * Map skeleton for C++ functions
     * @tparam Func  Type of the function to apply
     * @tparam ResultContainer The type of the result container
     * @tparam Ts Types of the arguments to pass to the function
     *
     * @param func The function to apply
     * @param result The result container
     * @param arguments Arguments to pass to the function
     */
    template <typename Func, typename ResultContainer, typename... Ts>
    void map(Func& func, ResultContainer& result, Ts&&... arguments) {

        for (std::size_t i = 0; i < result.size(); i++)
            result[i] = func(argument_get(std::forward<Ts>(arguments), i)...);

    }

    /**
     * Filter1 skeleton
     *
     * @tparam Func  Type of the function to apply
     * @tparam Container The type of the container to process
     * @tparam Ts Types of the arguments to pass to the function
     *
     * @param value Value to set
     * @param func The function to apply
     * @param container Container to process
     * @param arguments Arguments to pass to the function
     *
     * @return
     */
    template <typename Func, typename Container,  typename... Ts>
    auto filter1(typename Container::value_type value, Func& func, Container& container, Ts&&... arguments) {
        using ValueType = typename Container::value_type;

        const auto size = container.size();
        std::vector<ValueType> result (size);

        for (std::size_t i = 0; i < size; i++)
            result[i] = func(container[i], std::forward<Ts>(arguments)...) ? container[i] : value;

        return result;
    }

    /**
     * Filter2 skeleton
     *
     * @tparam Func  Type of the function to apply
     * @tparam Container The type of the container to process
     * @tparam Ts Types of the arguments to pass to the function
     *
     * @param func The function to apply
     * @param container Container to process
     * @param arguments Arguments to pass to the function
     *
     * @return
     */
    template <typename Func, typename Container,  typename... Ts>
    auto filter2(Func& func, Container& container, Ts&&... arguments) {
        using ValueType = typename Container::value_type;
        using LocatedValue = located_value<ValueType>;

        std::vector<LocatedValue> result;
        for (std::size_t i = 0; i < container.size(); i++) {
            if (func(container[i], std::forward<Ts>(arguments)...))
                result.push_back(LocatedValue { i, container[i] });
        };

        return result;
    }




    ////////////////////////// GPU

    
    template <typename Func, typename T, typename... Ts>
    __global__ void filter1_kernel(const unsigned size, T defaultVal, Func func, T* container, Ts... arguments){

        int index = blockIdx.x * blockDim.x + threadIdx.x;
        if(index < size){
            container[index] = func(container[index], arguments...) ?
                               container[index] : defaultVal;
        }
    }

    template <typename Func, typename Container,  typename... Ts>
    auto filter1_gpu(typename Container::value_type value, Func& func, Container& container, Ts&&... arguments)
    {
        using ValueType = typename Container::value_type;
        const auto size = container.size();
        std::vector<ValueType> result (size);

        constexpr auto ncontainers = count_containers<Ts...>()+1; // +1 para o container ^
        std::array<void *, ncontainers> gpu_addresses;

        allocate_gpu_mem(gpu_addresses, 0, container, std::forward<Ts>(arguments)...);
        copy_to_gpu(gpu_addresses, 0, container, std::forward<Ts>(arguments)...);

        Func d_func;
        cudaMemcpyFromSymbol(&d_func, func, sizeof(Func));

        int index = 1;
        const auto nb = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        filter1_kernel<<<nb, BLOCK_SIZE>>>(size, value, d_func, (ValueType*) gpu_addresses.at(0),
                                                  argument_get(std::forward<Ts>(arguments), gpu_addresses, index)...);

        cudaMemcpy(result.data(), gpu_addresses[0], size*sizeof(ValueType), cudaMemcpyDeviceToHost);

        free_gpu_mem(gpu_addresses);

        return result;
    }



/************************** Filter2 ***********************************************************************************/


    template <typename Func, typename T, typename... Ts>
    __global__ void compute_marks_kernel(const unsigned size, Func func, int * marks, T* container, Ts... arguments){

        int index = blockIdx.x * blockDim.x + threadIdx.x;
        if(index < size){
            marks[index] = func(container[index], arguments...); //implicit conversion - avoiding warp divergence
        }
    }

    template <typename Func, typename Container, typename Addrs, typename... Ts>
    auto compute_marks_gpu(Func& func,Container& container, Addrs& gpu_addresses, Ts&&... arguments)
    {
        using ValueType = typename Container::value_type;
        const auto size = container.size();

        Func d_func;
        int * d_marks; // output

        allocate_gpu_mem(gpu_addresses, 0, container, std::forward<Ts>(arguments)...);
        copy_to_gpu(gpu_addresses, 0, container, std::forward<Ts>(arguments)...);
        cudaMemcpyFromSymbol(&d_func, func, sizeof(Func));
        cudaMalloc((void**) &d_marks, size*sizeof(int));

        const auto nb = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        compute_marks_kernel<<<nb, BLOCK_SIZE>>>(size, d_func, d_marks,
                                                 (ValueType*) gpu_addresses[0],
                                                 argument_get(std::forward<Ts>(arguments), gpu_addresses, 1)...);

        return d_marks;
    }

    // Scans array already allocated and copied to the gpu
    template <typename T>
    T* scan_gpu(T* d_input, const unsigned size)
    {
        T * d_scan_output;
        cudaMalloc((void**) &d_scan_output, size*sizeof(T));

        scan<T> sc;
        sc.preallocBlockSums(size);
        sc.prescanArray(d_scan_output, d_input, size);
        sc.deallocBlockSums();

        return d_scan_output;
    }


    template <typename T>
    __global__ void fill_located_values(const T* __restrict__ original, const int* __restrict__ marks,
                                                const int* __restrict__ scanned_marks, unsigned original_size,
                                                T * result_values, std::size_t * result_coords)
    {
        int index = blockIdx.x * blockDim.x + threadIdx.x;
        if(index < original_size){
            if(marks[index]){
                int out_idx = scanned_marks[index];
                result_values[out_idx] = original[index];
                result_coords[out_idx] = index;
            }
        }
    }


    template <typename Func, typename Container,  typename... Ts>
    auto filter2_gpu(Func& func, Container& container, Ts&&... arguments)
    {
        using ValueType = typename Container::value_type;
        using LocatedValues = located_values<ValueType>;
        const auto size = container.size();
        constexpr auto ncontainers = count_containers<Ts...>() + 1;

        std::array<void *, ncontainers> gpu_addresses;
        int last_mark, last_scan_elem;
        LocatedValues result;

        /// 1) Compute array of marks ('booleans' resulting from applying func to each element of the container)
        int * d_marks = compute_marks_gpu(func, container, gpu_addresses, std::forward<Ts>(arguments)...);
        cudaMemcpy(&last_mark, &d_marks[size-1], sizeof(int), cudaMemcpyDeviceToHost);

        /// 2) Apply scan to marks; d_scanned_marks is a newly created array
        int * d_scanned_marks = scan_gpu(d_marks, size);
        cudaMemcpy(&last_scan_elem, &d_scanned_marks[size-1], sizeof(int), cudaMemcpyDeviceToHost);

        /// 3) Fill array with located values
        const auto final_array_size = last_scan_elem + last_mark; // exclusive scan, need to sum last mark
        std::size_t * d_result_coords;
        ValueType * d_result_values;

        cudaMalloc((void**) &d_result_coords, final_array_size*sizeof(std::size_t));
        cudaMalloc((void**) &d_result_values, final_array_size*sizeof(ValueType));
        const auto nb = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        fill_located_values<<<nb, BLOCK_SIZE>>>((ValueType*) gpu_addresses[0] , d_marks, d_scanned_marks,
                size, d_result_values, d_result_coords);

        std::vector<std::size_t> result_coords(final_array_size);
        std::vector<ValueType> result_values(final_array_size);
        cudaMemcpy(result_coords.data(), d_result_coords, final_array_size*sizeof(std::size_t), cudaMemcpyDeviceToHost);
        cudaMemcpy(result_values.data(), d_result_values, final_array_size*sizeof(ValueType), cudaMemcpyDeviceToHost);

        free_gpu_mem(gpu_addresses);
        cudaFree(d_marks); cudaFree(d_scanned_marks);
        cudaFree(d_result_coords); cudaFree(d_result_values);

        return LocatedValues {result_coords, result_values};
    }


    //////////// Async

    template <typename T>
    __global__ void fill_located_values1(const T* __restrict__ original, const int* __restrict__ marks,
                                         const int* __restrict__ scanned_marks, unsigned original_size,
                                         std::size_t * result_coords)
    {
        int index = blockIdx.x * blockDim.x + threadIdx.x;
        if(index < original_size){
            if(marks[index]){
                int out_idx = scanned_marks[index];
                result_coords[out_idx] = index;
            }
        }
    }

    template <typename T>
    __global__ void fill_located_values2(const T* __restrict__ original, const int* __restrict__ marks,
                                         const int* __restrict__ scanned_marks, unsigned original_size,
                                         T * result_values)
    {
        int index = blockIdx.x * blockDim.x + threadIdx.x;
        if(index < original_size){
            if(marks[index]){
                int out_idx = scanned_marks[index];
                result_values[out_idx] = original[index];
            }
        }
    }


    template <typename Func, typename Container,  typename... Ts>
    auto filter2_gpu_async(Func& func, Container& container, Ts&&... arguments)
    {
        using ValueType = typename Container::value_type;
        using LocatedValues = located_values2<ValueType>;
        const auto size = container.size();
        constexpr auto ncontainers = count_containers<Ts...>() + 1;

        std::array<void *, ncontainers> gpu_addresses;
        int last_mark, last_scan_elem;

        /// 1) Compute array of marks ('booleans' resulting from applying func to each element of the container)
        int * d_marks = compute_marks_gpu(func, container, gpu_addresses, std::forward<Ts>(arguments)...);
        cudaMemcpy(&last_mark, &d_marks[size-1], sizeof(int), cudaMemcpyDeviceToHost);

        /// 2) Apply scan to marks; d_scanned_marks is a newly created array
        int * d_scanned_marks = scan_gpu(d_marks, size);
        cudaMemcpy(&last_scan_elem, &d_scanned_marks[size-1], sizeof(int), cudaMemcpyDeviceToHost);

        /// 3) Fill arrays with located values
        const std::size_t final_array_size = last_scan_elem + last_mark; // exclusive scan, need to sum last mark
        std::size_t * d_result_coords, * h_result_coords;
        ValueType * d_result_values, * h_result_values;

        cudaMalloc((void**) &d_result_coords, final_array_size*sizeof(std::size_t));
        cudaMalloc((void**) &d_result_values, final_array_size*sizeof(ValueType));

        cudaStream_t stream1, stream2; // stream3;
        cudaStreamCreate( &stream1);
        cudaStreamCreate( &stream2);

        const auto nb = (size + BLOCK_SIZE - 1)/BLOCK_SIZE;
        fill_located_values1<<<nb, BLOCK_SIZE, 0, stream1>>>((ValueType*) gpu_addresses[0],
                                                             d_marks, d_scanned_marks,
                                                             size, d_result_coords);
        cudaMallocHost(&h_result_coords, final_array_size*sizeof(std::size_t)); // concurrent exec with kernel ^
        cudaMemcpyAsync(h_result_coords, d_result_coords, final_array_size*sizeof(std::size_t), cudaMemcpyDeviceToHost, stream1);

        fill_located_values2<<<nb, BLOCK_SIZE, 0, stream2>>>((ValueType*) gpu_addresses[0],
                                                             d_marks, d_scanned_marks,
                                                             size, d_result_values);
//        cudaMallocHost(&h_result_values, final_array_size*sizeof(ValueType)); <- not worth it;
        h_result_values = (ValueType *) malloc(final_array_size*sizeof(ValueType));
//        cudaMemcpyAsync(h_result_values, d_result_values, final_array_size*sizeof(ValueType), cudaMemcpyDeviceToHost, stream2);
        cudaMemcpy(h_result_values, d_result_values, final_array_size*sizeof(ValueType), cudaMemcpyDeviceToHost);

        cudaStreamSynchronize(stream1);
        cudaStreamSynchronize(stream2);

        free_gpu_mem(gpu_addresses);
        cudaFree(d_marks); cudaFree(d_scanned_marks);
        cudaFree(d_result_coords); cudaFree(d_result_values);
//        cudaFree(h_result_coords);

        return LocatedValues {h_result_coords, h_result_values, final_array_size};
    }


}
#endif //CADPROJECT_CUDA_SKELETONS_HPP
