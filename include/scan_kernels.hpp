
#ifndef CAD_CUDA_PROJECT_SCAN_KERNELS_HPP
#define CAD_CUDA_PROJECT_SCAN_KERNELS_HPP

//#define NUM_BANKS 32
//#define LOG_NUM_BANKS 5


//#ifdef ZERO_BANK_CONFLICTS
//#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS + (index) >> (2*LOG_NUM_BANKS))
//#else
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS)
//#endif

namespace cad {

    constexpr auto BLOCK_SIZE = 512; // TODO: ver qual o melhor BLOCK_SIZE
    constexpr auto NUM_BANKS = 32;
    constexpr auto LOG_NUM_BANKS = 5;


    template <typename T, bool isNP2>
    __device__ void loadSharedChunkFromMem(T* s_data,
                                           const T* __restrict__ g_idata,
                                           int n, int baseIndex,
                                           int& ai, int& bi,
                                           int& mem_ai, int& mem_bi,
                                           int& bankOffsetA, int& bankOffsetB)
    {
        int thid = threadIdx.x;
        mem_ai = baseIndex + threadIdx.x;
        mem_bi = mem_ai + blockDim.x;

        ai = thid;
        bi = thid + blockDim.x;

        // compute spacing to avoid bank conflicts
        bankOffsetA = CONFLICT_FREE_OFFSET(ai);
        bankOffsetB = CONFLICT_FREE_OFFSET(bi);

        // Cache the computational window in shared memory
        // pad values beyond n with zeros
        s_data[ai + bankOffsetA] = g_idata[mem_ai];

        if (isNP2) // compile-time decision
        {
            s_data[bi + bankOffsetB] = (bi < n) ? g_idata[mem_bi] : 0;
        }
        else
        {
            s_data[bi + bankOffsetB] = g_idata[mem_bi];
        }
    }

    template <typename T,bool isNP2>
    __device__ void storeSharedChunkToMem(T* g_odata,
                                          const T* __restrict__ s_data,
                                          int n,
                                          int ai, int bi,
                                          int mem_ai, int mem_bi,
                                          int bankOffsetA, int bankOffsetB)
    {
        __syncthreads();

        // write results to global memory
        g_odata[mem_ai] = s_data[ai + bankOffsetA];
        if (isNP2) // compile-time decision
        {
            if (bi < n)
                g_odata[mem_bi] = s_data[bi + bankOffsetB];
        }
        else
        {
            g_odata[mem_bi] = s_data[bi + bankOffsetB];
        }
    }


    template <typename T, bool storeSum>
    __device__ void clearLastElement(T* s_data,
                                     T* g_blockSums,
                                     int blockIndex)
    {
        if (threadIdx.x == 0)
        {
            int index = (blockDim.x << 1) - 1;
            index += CONFLICT_FREE_OFFSET(index);

            if (storeSum) // compile-time decision
            {
                // write this block's total sum to the corresponding index in the blockSums array
                // (it's done here because this is an exclusive scan; this is the only time this index holds the complete sum)
                g_blockSums[blockIndex] = s_data[index];
            }

            // zero the last element in the scan so it will propagate back to the front
            s_data[index] = 0;
        }
    }

    template <typename T>
    __device__ unsigned int buildSum(T* s_data)
    {
        unsigned int thid = threadIdx.x;
        unsigned int stride = 1;

        // build the sum in place up the tree
        for (int d = blockDim.x; d > 0; d >>= 1)
        {
            __syncthreads();

            if (thid < d)
            {
//                int i  = __mul24(__mul24(2, stride), thid);
                int i  = 2 * stride * thid;
                int ai = i + stride - 1;
                int bi = ai + stride;

                ai += CONFLICT_FREE_OFFSET(ai);
                bi += CONFLICT_FREE_OFFSET(bi);

                s_data[bi] += s_data[ai];
            }

            stride *= 2;
        }

        return stride;
    }

    template <typename T>
    __device__ void scanRootToLeaves(T* s_data, unsigned int stride)
    {
        unsigned int thid = threadIdx.x;

        // TODO: try to use shfl

        // traverse down the tree building the scan in place
        for (int d = 1; d <= blockDim.x; d *= 2)
        {
            stride >>= 1;

            __syncthreads();

            if (thid < d)
            {
//                int i  = __mul24(__mul24(2, stride), thid);
                int i  = 2 * stride * thid;
                int ai = i + stride - 1;
                int bi = ai + stride;

                ai += CONFLICT_FREE_OFFSET(ai);
                bi += CONFLICT_FREE_OFFSET(bi);

                T t  = s_data[ai];
                s_data[ai] = s_data[bi];
                s_data[bi] += t;
            }
        }
    }
    template <typename T, bool storeSum>
    __device__ void prescanBlock(T* data, int blockIndex, T* blockSums)
    {
        int stride = buildSum<T>(data);               // build the sum in place up the tree
        clearLastElement<T, storeSum>(data, blockSums,
                                   (blockIndex == 0) ? blockIdx.x : blockIndex);
        scanRootToLeaves<T>(data, stride);            // traverse down tree to build the scan
    }

    template <typename T, bool storeSum, bool isNP2>
    __global__ void prescan(T *g_odata,
                            const T* __restrict__ g_idata,
                            T *g_blockSums,
                            int n,
                            int blockIndex,
                            int baseIndex)
    {
        int ai, bi, mem_ai, mem_bi, bankOffsetA, bankOffsetB;
        extern __shared__ T s_data[];

        // load data into shared memory
        loadSharedChunkFromMem<T, isNP2>(s_data, g_idata, n,
                                      (baseIndex == 0) ? //__mul24(blockIdx.x, (blockDim.x << 1)):baseIndex,
                                      (blockIdx.x * (blockDim.x << 1)):baseIndex,
                                      ai, bi, mem_ai, mem_bi,
                                      bankOffsetA, bankOffsetB);
        // scan the data in each block
        prescanBlock<T, storeSum>(s_data, blockIndex, g_blockSums);
        // write results to device memory
        storeSharedChunkToMem<T, isNP2>(g_odata, s_data, n,
                                     ai, bi, mem_ai, mem_bi,
                                     bankOffsetA, bankOffsetB);
    }

    template <typename T>
    __global__ void uniformAdd(T* g_data,
                               T* uniforms,
                               int n,
                               int blockOffset,
                               int baseIndex)
    {
        __shared__ T uni;
        if (threadIdx.x == 0)
            uni = uniforms[blockIdx.x + blockOffset];

//        unsigned int address = __mul24(blockIdx.x, (blockDim.x << 1)) + baseIndex + threadIdx.x;
        unsigned int address = blockIdx.x * (blockDim.x << 1) + baseIndex + threadIdx.x;

        __syncthreads();

        // note two adds per thread
        g_data[address]              += uni;
        g_data[address + blockDim.x] += (threadIdx.x + blockDim.x < n) * uni;
    }


}

#endif //CAD_CUDA_PROJECT_SCAN_KERNELS_HPP
