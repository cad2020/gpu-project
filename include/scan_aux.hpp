

#ifndef CAD_CUDA_PROJECT_SCAN_AUX_HPP
#define CAD_CUDA_PROJECT_SCAN_AUX_HPP

#include <assert.h>
#include "scan_kernels.hpp"


/**
 * Macros from the original project; will probably delete them soon
 */



#  define CUT_CHECK_ERROR(errorMessage) do {                                 \
    cudaError_t err = cudaGetLastError();                                    \
    if( cudaSuccess != err) {                                                \
        fprintf(stderr, "Cuda error: %s in file '%s' in line %i : %s.\n",    \
                errorMessage, __FILE__, __LINE__, cudaGetErrorString( err) );\
        exit(EXIT_FAILURE);                                                  \
    }                                                                        \
    err = cudaDeviceSynchronize();                                           \
    if( cudaSuccess != err) {                                                \
        fprintf(stderr, "Cuda error: %s in file '%s' in line %i : %s.\n",    \
                errorMessage, __FILE__, __LINE__, cudaGetErrorString( err) );\
        exit(EXIT_FAILURE);                                                  \
    } } while (0)

#  define CUDA_SAFE_CALL( call) do {                                         \
    CUDA_SAFE_CALL_NO_SYNC(call);                                            \
    cudaError err = cudaDeviceSynchronize();                                 \
    if( cudaSuccess != err) {                                                \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
                __FILE__, __LINE__, cudaGetErrorString( err) );              \
        exit(EXIT_FAILURE);                                                  \
    } } while (0)

#  define CUDA_SAFE_CALL_NO_SYNC( call) do {                                 \
    cudaError err = call;                                                    \
    if( cudaSuccess != err) {                                                \
        fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
                __FILE__, __LINE__, cudaGetErrorString( err) );              \
        exit(EXIT_FAILURE);                                                  \
    } } while (0)



namespace cad {

    // TODO: onde é que isto fica??
    // constexpr auto BLOCK_SIZE = 256;
    // constexpr auto NUM_BANKS = 32;


    template <typename T>
    class scan {

    private:

        /**
         * Arrays to store intermediate block sums (one array per level)
         */
        T** g_scanBlockSums;

        /**
         * Number of elements of the original array to scan
         */
        unsigned int g_numEltsAllocated = 0;

        /**
         * // TODO comment this var
         */
        unsigned int g_numLevelsAllocated = 0;

        bool isPowerOfTwo(int n)
        {
            return ((n&(n-1))==0) ;
        }

        // arredonda para a potencia de 2 mais proxima de n (por baixo)
        int floorPow2(int n)
        {
            int exp;
            frexp((float)n, &exp);
            return 1 << (exp - 1);
        }

    public:

        void preallocBlockSums(unsigned int maxNumElements)
        {
//            assert(g_numEltsAllocated == 0); // shouldn't be called

            g_numEltsAllocated = maxNumElements;

            unsigned int blockSize = BLOCK_SIZE; // max size of the thread blocks
            unsigned int numElts = maxNumElements;

            int level = 0;

            do
            {
                unsigned int numBlocks =
                        max(1, (int)ceil((float)numElts / (2.f * blockSize)));
                if (numBlocks > 1)
                {
                    level++;
                }
                numElts = numBlocks;
            } while (numElts > 1);

            g_scanBlockSums = (T**) malloc(level * sizeof(T*));
            g_numLevelsAllocated = level;

            numElts = maxNumElements;
            level = 0;

            do
            {
                unsigned int numBlocks =
                        max(1, (int)ceil((float)numElts / (2.f * blockSize)));
                if (numBlocks > 1)
                {
                    CUDA_SAFE_CALL(cudaMalloc((void**) &g_scanBlockSums[level++],
                                              numBlocks * sizeof(T)));
                }
                numElts = numBlocks;
            } while (numElts > 1);

            //CUT_CHECK_ERROR("preallocBlockSums");
        }


        void deallocBlockSums()
        {
            for (int i = 0; i < g_numLevelsAllocated; i++)
            {
                cudaFree(g_scanBlockSums[i]);
            }

            //CUT_CHECK_ERROR("deallocBlockSums");

            free((void**)g_scanBlockSums);

            g_scanBlockSums = 0;
            g_numEltsAllocated = 0;
            g_numLevelsAllocated = 0;
        }



        void prescanArrayRecursive(T* outArray,
                                   const T* inArray,
                                   int numElements,
                                   int level)
        {
            unsigned int blockSize = BLOCK_SIZE; // max size of the thread blocks
            unsigned int numBlocks =
                    max(1, (int)ceil((float)numElements / (2.f * blockSize)));
            unsigned int numThreads;

            if (numBlocks > 1)
                numThreads = blockSize;
            else if (isPowerOfTwo(numElements))
                numThreads = numElements / 2;
            else
                numThreads = floorPow2(numElements);

            unsigned int numEltsPerBlock = numThreads * 2;

            // if this is a non-power-of-2 array, the last block will be non-full
            // compute the smallest power of 2 able to compute its scan.
            unsigned int numEltsLastBlock =
                    numElements - (numBlocks-1) * numEltsPerBlock;
            unsigned int numThreadsLastBlock = max(1, numEltsLastBlock / 2);
            unsigned int np2LastBlock = 0;
            unsigned int sharedMemLastBlock = 0;

            if (numEltsLastBlock != numEltsPerBlock)
            {
                np2LastBlock = 1;

                if(!isPowerOfTwo(numEltsLastBlock))
                    numThreadsLastBlock = floorPow2(numEltsLastBlock);

                unsigned int extraSpace = (2 * numThreadsLastBlock) / NUM_BANKS;
                sharedMemLastBlock =
                        sizeof(T) * (2 * numThreadsLastBlock + extraSpace);
            }

            // padding space is used to avoid shared memory bank conflicts
            unsigned int extraSpace = numEltsPerBlock / NUM_BANKS;
            unsigned int sharedMemSize =
                    sizeof(T) * (numEltsPerBlock + extraSpace);


            // setup execution parameters
            // if NP2, we process the last block separately
            dim3  grid(max(1, numBlocks - np2LastBlock), 1, 1);
            dim3  threads(numThreads, 1, 1);

            // make sure there are no CUDA errors before we start
            //CUT_CHECK_ERROR("prescanArrayRecursive before kernels");


            // execute the scan
            if (numBlocks > 1)
            {
                prescan<T, true, false><<< grid, threads, sharedMemSize >>>(outArray,
                        inArray,
                        g_scanBlockSums[level],
                        numThreads * 2, 0, 0);
                //CUT_CHECK_ERROR("prescanWithBlockSums");
                if (np2LastBlock)
                {
                    prescan<T, true, true><<< 1, numThreadsLastBlock, sharedMemLastBlock >>>
                    (outArray, inArray, g_scanBlockSums[level], numEltsLastBlock,
                            numBlocks - 1, numElements - numEltsLastBlock);
                    //CUT_CHECK_ERROR("prescanNP2WithBlockSums");
                }

                // After scanning all the sub-blocks, we are mostly done.  But now we
                // need to take all of the last values of the sub-blocks and scan those.
                // This will give us a new value that must be sdded to each block to
                // get the final results.
                // recursive (CPU) call
                prescanArrayRecursive(g_scanBlockSums[level],
                                      g_scanBlockSums[level],
                                      numBlocks,
                                      level+1);

                uniformAdd<T><<< grid, threads >>>(outArray,
                        g_scanBlockSums[level],
                        numElements - numEltsLastBlock,
                        0, 0);
                //CUT_CHECK_ERROR("uniformAdd");
                if (np2LastBlock)
                {
                    uniformAdd<T><<< 1, numThreadsLastBlock >>>(outArray,
                            g_scanBlockSums[level],
                            numEltsLastBlock,
                            numBlocks - 1,
                            numElements - numEltsLastBlock);
                    //CUT_CHECK_ERROR("uniformAdd");
                }
            }
            else if (isPowerOfTwo(numElements))
            {
                prescan<T, false, false><<< grid, threads, sharedMemSize >>>(outArray, inArray,
                        0, numThreads * 2, 0, 0);
                //CUT_CHECK_ERROR("prescan");
            }
            else
            {
                prescan<T, false, true><<< grid, threads, sharedMemSize >>>(outArray, inArray,
                        0, numElements, 0, 0);
                //CUT_CHECK_ERROR("prescanNP2");
            }
        }


        // Assumes outArray and inArray have already been copied to the gpu
        void prescanArray(T *outArray, T *inArray, int numElements)
        {
            prescanArrayRecursive(outArray, inArray, numElements, 0);
        }

    };











}





#endif //CAD_CUDA_PROJECT_SCAN_AUX_HPP
