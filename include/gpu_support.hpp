
#ifndef CAD_CUDA_PROJECT_GPU_SUPPORT_HPP
#define CAD_CUDA_PROJECT_GPU_SUPPORT_HPP


namespace cad {

    template <typename T1, typename T2, typename... Ts>
    constexpr unsigned count_containers() {
        return (not std::is_fundamental<std::decay_t<T1>>::value) + count_containers<T2, Ts...>();
    }

    template <typename T>
    constexpr unsigned count_containers() {
        return not std::is_fundamental<std::decay_t<T>>::value;
    }


    //////////////// Allocate space in the gpu for an unknown sequence of inputs (only for the containers)

    template <typename Addrs>
    static void allocate_gpu_mem(Addrs&, unsigned) { }

    template <typename Addrs, typename T, typename... Ts,
            std::enable_if_t<std::is_fundamental<std::decay_t<T>>::value>* = nullptr>
    static void allocate_gpu_mem(Addrs& gpu_addresses, unsigned index, T&& a, Ts&&... arguments) {
        allocate_gpu_mem(gpu_addresses, index, arguments...);
    }

    template <typename Addrs, typename T, typename... Ts,
            std::enable_if_t<not std::is_fundamental<std::decay_t<T>>::value>* = nullptr>
    static void allocate_gpu_mem(Addrs& gpu_addresses, unsigned index, T&& a, Ts&&... arguments) {

        const auto size_in_bytes = a.size() * sizeof(typename std::decay_t<T>::value_type);
        cudaMalloc((void**) &gpu_addresses[index], size_in_bytes);
        allocate_gpu_mem(gpu_addresses, index+1, arguments...);
    }


    //////////////// Copy unknown sequence of inputs to the gpu (only the containers)

    template <typename Addrs>
    static void copy_to_gpu(Addrs&, unsigned) { }

    template <typename Addrs, typename T, typename... Ts,
            std::enable_if_t<std::is_fundamental<std::decay_t<T>>::value>* = nullptr>
    static void copy_to_gpu(Addrs& gpu_addresses, unsigned index, T&& a, Ts&&... arguments) {
        copy_to_gpu(gpu_addresses, index, arguments...);
    }

    template <typename Addrs, typename T, typename... Ts,
            std::enable_if_t<not std::is_fundamental<std::decay_t<T>>::value>* = nullptr>
    static void copy_to_gpu(Addrs& gpu_addresses, unsigned index, T&& a, Ts&&... arguments) {

        const auto size_in_bytes = a.size() * sizeof(typename std::decay_t<T>::value_type);
        cudaMemcpy(gpu_addresses[index], a.data(), size_in_bytes, cudaMemcpyHostToDevice);
        copy_to_gpu(gpu_addresses, index+1, arguments...);
    }



    template <typename Addrs, typename T, std::enable_if_t<std::is_fundamental<T>::value>* = nullptr>
    constexpr T argument_get(T argument, Addrs& gpu_addresses, std::size_t index) {
        return argument;
    }

    template <typename Addrs, typename T, std::enable_if_t<not std::is_fundamental<T>::value>* = nullptr>
    constexpr auto argument_get(T&& argument, Addrs& gpu_addresses, std::size_t& index) {
        return gpu_addresses[index++];
    }


    template <typename Addrs>
    static void free_gpu_mem(Addrs& gpu_addresses) {
        for(const auto& addr : gpu_addresses )
            cudaFree(addr);
    }

}


#endif //CAD_CUDA_PROJECT_GPU_SUPPORT_HPP
